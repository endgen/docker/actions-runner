FROM ubuntu:24.04

RUN apt update
RUN apt upgrade -y
RUN apt install curl libdigest-sha-perl -y

RUN userdel -r ubuntu
RUN groupadd -g 120 docker
RUN useradd -u 1000 -U -m actions-runner
RUN usermod -a -G docker actions-runner

USER actions-runner

WORKDIR /usr/local/lib/actions-runner

RUN curl -o actions-runner-linux-x64-2.311.0.tar.gz -L https://github.com/actions/runner/releases/download/v2.311.0/actions-runner-linux-x64-2.311.0.tar.gz
RUN echo "29fc8cf2dab4c195bb147384e7e2c94cfd4d4022c793b346a6175435265aa278 *actions-runner-linux-x64-2.311.0.tar.gz" | shasum -a 256 -c
RUN tar xzf ./actions-runner-linux-x64-2.311.0.tar.gz
RUN rm -rf ./actions-runner-linux-x64-2.311.0.tar.gz

USER root

RUN ./bin/installdependencies.sh

RUN ln -s /usr/local/lib/actions-runner/config.sh /usr/bin/config
RUN ln -s /usr/local/lib/actions-runner/run.sh /usr/bin/run

RUN apt remove curl -y

USER actions-runner

CMD sh -c "while [ ! -f /usr/local/lib/actions-runner/.credentials ]; do sleep 10; done && sleep 15 && run"
